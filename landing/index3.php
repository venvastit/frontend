<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="description" content="VenVast Every Day is Event Day is a Social Event Management Platform as aspires to change events">
<meta name="keywords" content="VenVast, Events, Social, Management, Platform, Organizer, User, Host, Business, ">
<meta name="author" content="VenVast">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- SITE TITLE -->
<title>VenVast - Every Day is Event Day</title>

<!-- =========================
      FAV AND TOUCH ICONS  
============================== -->
<link rel="icon" href="images/favicon.ico">
<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">

<!-- =========================
     STYLESHEETS   
============================== -->
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="css/bootstrap.min.css">

<!-- FONT ICONS -->
<!-- IonIcons -->
<link rel="stylesheet" href="assets/ionicons/css/ionicons.css">

<!-- Font Awesome 
<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
-->

<!-- Elegant Icons -->
<link rel="stylesheet" href="assets/elegant-icons/style.css">
<!--[if lte IE 7]><script src="assets/elegant-icons/lte-ie7.js"></script><![endif]-->

<!-- CAROUSEL AND LIGHTBOX -->
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/nivo-lightbox.css">
<link rel="stylesheet" href="css/nivo_themes/default/default.css">

<!-- COLORS -->
<link rel="stylesheet" href="css/colors/blue.css"> <!-- DEFAULT COLOR/ CURRENTLY USING -->
<!-- <link rel="stylesheet" href="css/colors/red.css"> -->
<!-- <link rel="stylesheet" href="css/colors/green.css"> -->
<!-- <link rel="stylesheet" href="css/colors/purple.css"> -->
<!-- <link rel="stylesheet" href="css/colors/orange.css"> -->
<!-- <link rel="stylesheet" href="css/colors/blue-munsell.css"> -->
<!-- <link rel="stylesheet" href="css/colors/slate.css"> -->
<!-- <link rel="stylesheet" href="css/colors/yellow.css"> -->

<!-- CUSTOM STYLESHEETS -->
<link rel="stylesheet" href="css/styles.css">

<!-- RESPONSIVE FIXES -->
<link rel="stylesheet" href="css/responsive.css">

<!--[if lt IE 9]>
			<script src="js/html5shiv.js"></script>
			<script src="js/respond.min.js"></script>
<![endif]-->

<!-- ****************
      After neccessary customization/modification, Please minify HTML/CSS according to http://browserdiet.com/en/ for better performance
     **************** -->
     
</head>

<body>
<!-- =========================
     PRE LOADER       
============================== -->
<!-- <div class="preloader">
  <div class="status">&nbsp;</div>
</div> --> 
<!-- =========================
     HEADER   
============================== -->
<header id="home">

<!-- COLOR OVER IMAGE -->
<div class="color-overlay">
	
	<div class="navigation-header">
		
		<!-- STICKY NAVIGATION -->
		<div class="navbar navbar-inverse bs-docs-nav navbar-fixed-top sticky-navigation">
			<div class="container">
				<div class="navbar-header">
					
					<!-- LOGO ON STICKY NAV BAR -->
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#landx-navigation">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><img src="images/logo-dark.png" alt=""></a>
					
				</div>
				
				<!-- NAVIGATION LINKS -->
				<div class="navbar-collapse collapse" id="landx-navigation">
					<ul class="nav navbar-nav navbar-right main-navigation">
						<li><a href="#home">Home</a></li>
						<li><a href="#section1">Features</a></li>
						<li><a href="#section2">About</a></li>
						<li><a href="#section3">Contact</a></li>
					</ul>
				</div>
				
			</div>
			<!-- /END CONTAINER -->
			
		</div>
		
		<!-- /END STICKY NAVIGATION -->
		
		<!-- ONLY LOGO ON HEADER -->
		<div class="navbar non-sticky">
			
			<div class="container">
				
				<div class="navbar-header">
					<img src="images/logo.png" alt="">
				</div>
				
				<ul class="nav navbar-nav navbar-right social-navigation hidden-xs">
					<li><a href="http://facebook.com/venvast"><i class="social_facebook_circle"></i></a></li>
					<li><a href="http://twitter.com/VenVast"><i class="social_twitter_circle"></i></a></li>
						</ul>
			</div>
			<!-- /END CONTAINER -->
			
		</div>
		<!-- /END ONLY LOGO ON HEADER -->
		
	</div>
	
	<!-- HEADING, FEATURES AND REGISTRATION FORM CONTAINER -->
	<div class="container">
		
		<div class="row">
			
			<!-- LEFT - HEADING AND TEXTS -->
			<div class="col-md-7 col-sm-7 intro-section">
				
				<h1 class="intro text-left">
				Every <strong> Day </strong> is Event <strong> Day</strong> 
				</h1>
				
				<ul class="feature-list-1">
					
					<!-- FEATURE ROW -->
					<li>
					<div class="icon-container pull-left">
						<span class="icon_check"></span>
					</div>
					<p class="text-left">
						VenVast is a Social Event Management Platform that aspires to solve day to day problems that people face when trying to find events that will interest them.
					</p>
					</li>
					
					<!-- FEATURE ROW -->
					<li>
					<div class="icon-container pull-left">
						<span class="icon_check"></span>
					</div>
					<p class="text-left">
					Users and Event Organizers/Hosts will get features and tools to help them in their day to day tasks.
					</p>
					</li>
					
					<!-- FEATURE ROW -->
					<li>
					<div class="icon-container pull-left">
						<span class="icon_check"></span>
					</div>
					<p class="text-left">
					VenVast has communicated with many local businesses and made partnerships with a wide variety of individuals and organizations.
					</p>
					</li>
				</ul>
				
			</div>
			
			<!-- RIGHT - REGISTRATION FORM -->
			
			<div class="col-md-5 col-sm-5">
				
		<div class="vertical-registration-form">
					<div class="colored-line">
					</div>
					<h3>Get your VIP Membership now!</h3>
                    <form class="subscription-form mailchimp" role="form">
				
				<!-- SUBSCRIPTION SUCCESSFUL OR ERROR MESSAGES -->
				
					<!-- <form class="registration-form" id="register" role="form"> -->
                    <input type="name" name="name" id="subscriber-name" placeholder="Your Name" class="form-control input-box" style="color:white">
						<input type="email" name="email" id="subscriber-email" placeholder="Your Email" class="form-control input-box" style="color:white">
							<button type="submit" id="subscribe-button" class="btn standard-button" data-toggle="modal" href="#myModal" >Send</button>
					<!--	<input class="form-control input-box" id="name" type="text" name="name" placeholder="Your Name">
						<input class="form-control input-box" id="email" type="email" name="email" placeholder="Your Email">
						<button class="btn standard-button" type="submit" id="submit" name="submit">Send</button> --> 
                        <!--data-toggle="modal" href="#myModal" -->
					</form>
					<h5 class="register-success" style="color:white;">Message sent successfully</h5>
					<h5 class="register-error" style="color:white;">Error! Please fill all field correctly</h5>
				</div>
			</div>
			<!-- /END - REGISTRATION FORM -->
		</div>
		
	</div>
	<!-- /END HEADING, FEATURES AND REGISTRATION FORM CONTAINER -->
	
</div>

</header>


<!-- =========================
     SECTION 1   
============================== -->
<section class="section3" id="section1">

<div class="container">
	
	<!-- SECTION HEADING -->
	
	<h2>VenVast Features</h2>
	
	<div class="colored-line">
	</div>
	
	<div class="sub-heading">
	
	</div>
	
	<div class="features">
		
		<!-- FEATURES ROW 1 -->
		<div class="row">
			
			<!-- SINGLE FEATURE BOX -->
			<div class="col-md-4">
				<div class="feature">
					<div class="icon">
						<span class="icon_calendar"></span>
					</div>
					<h4>Advanced Calendar</h4>
					<p>
						Craft you day, Create your day, Consume your day, with our advanced calendar. Love the Calendar again. Connect our Calendar with your favorite existing Calendar.
					</p>
				</div>
			</div>
			
			<!-- SINGLE FEATURE BOX -->
			<div class="col-md-4">
				<div class="feature">
					<div class="icon">
						<span class="icon_tag_alt"></span>
					</div>
					<h4>Your Interests</h4>
					<p>
						Your Interests are at the core of what VenVast is. When you select your Interests, our system tailors to your wants and needs. We do the hard work for you, so you dont have too.
					</p>
				</div>
			</div>
			
			<!-- SINGLE FEATURE BOX -->
			<div class="col-md-4">
				<div class="feature">
					<div class="icon">
						<span class="icon_chat_alt"></span>
					</div>
					<h4>I2I Chat</h4>
					<p>
						I2I Chat is a advanced chat system, that is purpose built for what we do at VenVast. Pre, During and Post Chat. To get more interactive with the social community that is VenVast.
					</p>
				</div>
			</div>
		</div>
		
		<!-- FEATURES ROW 2 -->
		<div class="row">
			
			<!-- SINGLE FEATURE BOX -->
			<div class="col-md-4">
				<div class="feature">
					<div class="icon">
						<span class="icon_lock_alt"></span>
					</div>
					<h4>Memory Locker</h4>
					<p>
			Every Event has a memory that is worth a thousand words, special moment, Every Event has a memory locker. Capture, Upload and share your memories to the cloud, so no memory will be forgotten at your event. 
					</p>
				</div>
			</div>
			
			<!-- SINGLE FEATURE BOX -->
			<div class="col-md-4">
				<div class="feature">
					<div class="icon">
						<span class="icon_heart_alt"></span>
					</div>
					<h4>Favorites</h4>
					<p>
						Save your favorites, events, event organizers, media and much more. Your favorites are awaiting you. 
					</p>
				</div>
			</div>
			
			<!-- SINGLE FEATURE BOX -->
			<div class="col-md-4">
				<div class="feature">
					<div class="icon">
						<span class="icon_group"></span>
					</div>
					<h4>For the User and the Organizer</h4>
					<p>
					

We want to be the most simple, easy and fast platform on the web. For the User the interests will automatically intertwine with the system to provide with the best experience. For the organizer we provide many premium tools to help them achieve their hopes and goals.

					</p>
				</div>
			</div>
		</div> 
	</div>
	
</div> <!-- /END CONTAINER -->
</section>

<!-- =========================
     SECTION 2   
============================== -->
<section class="section1" id="section2">

<div class="container">
	<div class="row">
		
		<div class="col-md-6">
			
			<!-- SCREENSHOT -->
			<div class="side-screenshot pull-left">
				<img src="images/image-1.jpg" alt="Feature" class="img-responsive">
			</div>
			
		</div>
		
		<div class="col-md-6">
			
			<!-- DETAILS WITH LIST -->
			<div class="brief text-left">
				
				<!-- HEADING -->
				<h2>About VenVast</h2>
				<div class="colored-line pull-left">
				</div>
				
				<!-- TEXT -->
				<p>
		<strong>VenVast</strong> - is a Social Event Management Platform that aspires to solve day to day problems that people face when trying to find events that will interest them. By providing many different functionalities, VenVast paves a better path between organizers and users, allowing users to not only attend already organized events, but to also craft their days by organizing their own events. With VenVast, every day is event day.
				</p>
                <p>
               <strong>Mission</strong> - is to revolutionize the way event management platforms work by providing a one stop solution at the click of a button. VenVast sees that we enrich ourselves with events every day, whether they are scheduled or not, which is where organic events formalize and help users craft their day, ultimately leading to our Social Event Management Platform where all can stay connected with likeminded and fellow interested users. With VenVast’s function of pre, during, and post event chats, we enable users to engage and gain in-depth knowledge regarding events. Additionally, with users selecting interests, it offers the opportunity to personalize their experiences in an even greater depth.
               </p>
               <p> <strong>Vision</strong> - to be the first ever worldwide fully executed social event management platform, which will follow organizers and attendees’ interests, because with VenVast every day is event day
				</p>	
                
						
	</div> <!-- END ROW -->
	
</div> <!-- END CONTAINER -->

</section>

<!-- =========================
     SECTION 3 - CTA  
============================== -->
<section class="cta-section" id="section3">
<div class="color-overlay">
	
	<div class="container">
		
		<h4>Interest?</h4>
		<h2>Get your VIP membership now!</h2>
		<div id="cta-4">
			<a href="#home" class="btn standard-button"> Get Started</a>
		</div>
		
		<!-- MAILCHIMP SUBSCRIBE FORM / REMOVE 'hidden' CLASS TO MAKE FORM VISIBLE -->
		
		<div class="subscribe-section hidden">
			
			<h3>Or, Mailchimp Subscription Form</h3>
			
			<form class="subscription-form mailchimp form-inline" role="form">
				
				<!-- SUBSCRIPTION SUCCESSFUL OR ERROR MESSAGES -->
				<h6 class="subscription-success"></h6>
				<h6 class="subscription-error"></h6>
				
				<!-- EMAIL INPUT BOX -->
				<input type="email" name="email" id="subscriber-email" placeholder="Your Email" class="form-control input-box">
				
				<!-- SUBSCRIBE BUTTON -->
				<button type="submit" id="subscribe-button" class="btn standard-button">Subscribe</button>
				
			</form>
		</div>
		<!-- /END SUBSCRIPTION FORM -->
		
	</div>  <!-- /END CONTAINER -->
</div>  <!-- /END COLOR OVERLAY -->

</section>

<!-- =========================
     SECTION 4 - CONTACT US  
============================== -->
<section class="contact-us" id="section4">
<div class="container">
	
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			
			<h3 class="heading">Want to get intouch? Contact us now!</h3>
			
			<a href="" class="contact-link expand-form"><span class="icon_mail_alt"></span>Contact us now</a>
			
			<!-- EXPANDED CONTACT FORM -->
			<div class="expanded-contact-form">
				
				<!-- FORM -->
				<form class="contact-form" id="contact" role="form">
					
					<!-- IF MAIL SENT SUCCESSFULLY -->
					<h6 class="success">
					<span class="olored-text icon_check"></span> Your message has been sent successfully. </h6>
					
					<!-- IF MAIL SENDING UNSUCCESSFULL -->
					<h6 class="error">
					<span class="colored-text icon_error-circle_alt"></span> E-mail must be valid and message must be longer than 1 character. </h6>
					
					<div class="field-wrapper col-md-6">
						<input class="form-control input-box" id="cf-name" type="text" name="cf-name" placeholder="Your Name">
					</div>
					
					<div class="field-wrapper col-md-6">
						<input class="form-control input-box" id="cf-email" type="email" name="cf-email" placeholder="Email">
					</div>
					
					<div class="field-wrapper col-md-12">
						<input class="form-control input-box" id="cf-subject" type="text" name="cf-subject" placeholder="Subject">
					</div>
					
					<div class="field-wrapper col-md-12">
						<textarea class="form-control textarea-box" id="cf-message" rows="7" name="cf-message" placeholder="Your Message"></textarea>
					</div>
					
					<button class="btn standard-button" type="submit" id="cf-submit" name="submit" data-style="expand-left">Send Message</button>
				</form>
				<!-- /END FORM -->
			</div>
			
		</div>
	</div>
	
</div>

</section>
<!-- =========================
     SECTION 5 - FOOTER 
============================== -->
<footer class="bgcolor-2">
<div class="container">
	
	<div class="footer-logo">
		<img src="images/logo-dark.png" alt="">
	</div>
	
	<div class="copyright">
		 ©2016 VenVast.
	</div>
	
	<ul class="social-icons">
		<li><a href="http://facebook.com/venvast"><span class="social_facebook_square"></span></a></li>
 		<li><a href="http://twitter.com/VenVast"><span class="social_twitter_square"></span></a></li>
		</ul>
	
</div>
</footer>


<!-- =========================
     SCRIPTS   
============================== -->
<script src="js/jquery.min.js"></script>

<script>
/* =================================
   LOADER                     
=================================== */
// makes sure the whole site is loaded
jQuery(window).load(function() {
	"use strict";
        // will first fade out the loading animation
	jQuery(".status").fadeOut();
        // will fade out the whole DIV that covers the website.
	jQuery(".preloader").delay(1000).fadeOut("slow");
})

</script>

<script src="js/bootstrap.min.js"></script>
<script src="js/retina-1.1.0.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/jquery.scrollTo.min.js"></script>
<script src="js/jquery.localScroll.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/nivo-lightbox.min.js"></script>
<script src="js/simple-expand.min.js"></script>
<script src="js/jquery.nav.js"></script>
<script src="js/jquery.fitvids.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/bootbox.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-75041644-1', 'auto');
  ga('send', 'pageview');

</script>
<script> 
$(function(){
    $('#myModal').on('show.bs.modal', function(){
        var myModal = $(this);
        clearTimeout(myModal.data('hideInterval'));
        myModal.data('hideInterval', setTimeout(function(){
            myModal.modal('hide');
        }, 4000));
    });
});
</script>
 
 

<!-- modal --> 
<div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
             
            </div>
            <div class="modal-body">
               <h6 class="subscription-success" style="color:black"></h6>
				<h6 class="subscription-error" style="color:black"></h6>
            </div>
        </div> 
    </div>
</div>
<!-- ****************
      After neccessary customization/modification, Please minify JavaScript/jQuery according to http://browserdiet.com/en/ for better performance
     **************** -->
</body>
</html>