$(".slideshow-bg main.background").vegas({
	delay: 3000,
    slides: [
        { src: "images/_MG_7946.jpg" },
        { src: "images/IMG_5558-2.jpg" },
        { src: "images/IMG_5703-2.jpg" },
        { src: "images/IMG_2418.jpg" },
        { src: "images/IMG_5672-2.jpg" },
        { src: "images/IMG_5682.jpg" },
        { src: "images/IMG_5726.jpg" },
        { src: "images/IMG_1132-2.jpg" }
    ]
});