<?php
// Email Submit
// Note: filter_var() requires PHP >= 5.2.0




$message = "Congratulations valued member! You are officially one of the 500 users to have received a lifetime VIP membership from VenVast. Stay tuned for more updates and remember: With VenVast, every day is event day!";

if(!isset($_POST['name']) || !isset($_POST['email']))
  die();

$dbh = new PDO('mysql:host=localhost;dbname=landing', 'root', 'cm2jpmrt6c');
$existingSameEmails = [];
foreach($dbh->query('SELECT * FROM users WHERE email="'.$_POST['email'].'"') as $row){
  $existingSameEmails[] = $row;
}
if(count($existingSameEmails) > 0){
  echo "error";
  die();
}
// var_dump(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL));
// var_dump(isset($_POST['email']) && isset($_POST['name']));
if ( isset($_POST['email']) && isset($_POST['name']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
 
  // detect & prevent header injections
  $test = "/(content-type|bcc:|cc:|to:)/i";
  foreach ( $_POST as $key => $val ) {
    if ( preg_match( $test, $val ) ) {
      exit;
    }
  }


$sql = "INSERT INTO users(email, name) VALUES(:email, :name)";
$stmt = $dbh->prepare($sql);
$stmt->bindParam(':email', $_POST['email'], PDO::PARAM_STR);
$stmt->bindParam(':name', $_POST['name'], PDO::PARAM_STR);
$stmt->execute();



$message = [];
$message['text'] = $message;
$message['html'] = file_get_contents('./email.html');
$message['subject'] = "Thank you for your interest! VenVast";
$message['from_email'] = "info@venvast.com";
$message['from_name']  = "VanVast team";
$message['track_opens'] = true;
$message['to'] = [[ 
                'email' => $_POST['email'],
                'name' => $_POST['name'],
                'type' => 'to']];

$uri = 'https://mandrillapp.com/api/1.0/messages/send.json';
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $uri);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode(['key' => 't748Dti01CJJFnOHPrIvaQ', 'message' => $message]));

$result = curl_exec($ch);

echo $result;


}
?>